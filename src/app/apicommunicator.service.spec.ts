import { TestBed } from '@angular/core/testing';

import { ApicommunicatorService } from './apicommunicator.service';

describe('ApicommunicatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApicommunicatorService = TestBed.get(ApicommunicatorService);
    expect(service).toBeTruthy();
  });
});
