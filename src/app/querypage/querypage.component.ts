import { Component, OnInit } from '@angular/core';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { ApicommunicatorService} from '../apicommunicator.service';
import { Router} from '@angular/router';
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';
import { removeSummaryDuplicates } from '@angular/compiler';

@Component({
  selector: 'app-querypage',
  templateUrl: './querypage.component.html',
  styleUrls: ['./querypage.component.css']
})
export class QuerypageComponent implements OnInit {


  //Functional Overview
  /*
  filterobjects stores the JSON object array that is parsed and eventually sent to the API in
  a form that is used to dynamically construct the DB query.

  Once the API returns a result in JSON array format, an *ngFor iterates over each object to
  construct the "Results" list.

  This approach stores all the results in memory which, with a blank query and the current dataset is ~10MB
  This is not ideal but can be improved upon with a paginated "Results" display component.

  I have left other suggestions as comments as to how one would improve certain function and features.
  */
  private filterobjects = [];
  private searchlist = [];
  private URL = "www.google.com";
  private filters = [];
  private results = [];
  private url;
  private searchfiltered = [];
 
  constructor(
    private apiComm: ApicommunicatorService,
    private router: Router,
    public sanitizer:DomSanitizer
  ) { }
  
  ngOnInit() {
    //Retrieve filters from APICOMM service
    this.filters = this.apiComm.getFilters();
    //Setting default position for live map
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.apiComm.getMap("South+Africa+Gauteng"));
  }

  addfilter(item)
  {
    //Adds a new filter if it doesn't exist already when one is selected from the dropdown, default value is assigned in the model
    if(!this.filterobjects.includes(item))
    this.filterobjects.push(item);
  }

  openMap(param1, param2, param3)
  {
    //Updates live map with new area
    var link = this.apiComm.getMap(param1+","+param2+","+param3+","+"South+Africa");
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(link);
  }

  //Opoens Google Maps URL
  openGMap(param1, param2, param3)
  {
    var link;
    this.apiComm.getGMap(param1+","+param2+","+param3+","+"South+Africa")
    .then(result =>{ 
      link = result["url"];
      console.log(result)
      window.location.href = "https://"+link;
    })
    .catch(error => {console.log(error)})
    
  }

  //Unhides the "Details" HTML element
  //Generating all the details elements and hiding them is bad for performance but will be made dynamic if POC is taken further 
  moreDetails(object)
  {
    if(document.getElementById(object["OBJECTID"]).style.display == "none")
    document.getElementById(object["OBJECTID"]).style.display = ("block");
    else
    document.getElementById(object["OBJECTID"]).style.display = ("none");
  }

  //Passes list of search fields and values to APICOMM service
  //Waits for response and then updates "local" list of results
  newquery()
  {
    this.apiComm.getResults(this.filterobjects)
    .then(result => { console.log(result); this.results = result})
    .catch(error => { console.log(error)})
    console.log(this.results);
  }

  //Triggers when a value for a filter is changed, this happens on change which is triggered after the input is unfocused hence it's not THAT bad for performance
  //A dynamic form would be better
  updatevalue(item, newvalue)
  {
    
    for(var i =0; i < this.filterobjects.length; i++)
    {
      if(this.filterobjects[i] === item)
      {
        this.filterobjects[i].fieldvalue = newvalue;
      }
    }
    console.log(this.filterobjects)
  }

  //Inverse of the addfilter() function
  removefilter(item)
  {
    for(var i =0; i < this.filterobjects.length; i++)
    {
      if(this.filterobjects[i] === item)
      {
        this.filterobjects.splice(i,1)
      }
    }
  }
 

}
