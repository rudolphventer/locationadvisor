import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApicommunicatorService {

  //Defining "Model", this should be done in another way but not for this POC
  private criteria = [
{name: "Province", fieldname: "ProvinceName", fieldvalue:"name", fieldtype:"dropdownComponent", values: [{value: "Eastern Cape"},{value: "Free State"},{value: "Gauteng"},{value: "KwaZulu-Natal"},{value: "Limpopo"},{value: "Mpumalanga"},{value: "Northern Cape"},{value: "North West"},{value: "Western Cape"}]},
{name: "Local Municipality", fieldname: "LocalMunicipalityName", fieldvalue:"name", fieldtype:"textboxComponent"},
{name: "District Municipality", fieldname: "DistrictMunicipalityName", fieldvalue:"name", fieldtype:"textboxComponent"},
{name: "Average Age Range", fieldname: "AverageAge", fieldvalue:"name", fieldtype:"rangesliderComponent"},
{name: "Income", fieldname: "mostcommonincome", fieldvalue:"name", fieldtype:"dropdownComponent", values: [{value: "No income"},{value: "R 1 - R 4800"},{value: "R 4801 - R 9600"},{value: "R 9601 - R 19200"},{value: "R 19201 - R 38400"},{value: "R 38401 -  R 76800"},{value: "R 76801 - R 153600"},{value: "R 153601 - R 307200"},{value: "R 307201 - R 614400"},{value: "R 614401- R 1228800"},{value: "R 1228801 - R 2457600"},{value: "R2457601 or more"}]},
{name: "Area Name", fieldname: "placename", fieldvalue:"name", fieldtype:"textboxComponent"}
  ]
  private criteriafull = [
    {fieldname: "ProvinceCode", fieldvalue:"name", fieldtype:"dropdownComponent"},
    {fieldname: "ProvinceName", fieldvalue:"name", fieldtype:"dropdownComponent", values: [{value: "Eastern Cape"},{value: "Free State"},{value: "Gauteng"},{value: "KwaZulu-Natal"},{value: "Limpopo"},{value: "Mpumalanga"},{value: "Northern Cape"},{value: "North West"},{value: "Western Cape"}]},
    {fieldname: "LocalMunicipalityCode", fieldtype:"textboxComponent"},
    {fieldname: "WardNumber", fieldvalue:"name", fieldtype:"textboxComponent"},
    {fieldname: "WardID", fieldvalue:"name", fieldtype:"textboxComponent"},
    {fieldname: "LocalMunicipalityName", fieldvalue:"name", fieldtype:"textboxComponent"},
    {fieldname: "DistrictMunicipalityCode", fieldvalue:"name", fieldtype:"textboxComponent"},
    {fieldname: "DistrictMunicipalityName", fieldvalue:"name", fieldtype:"textboxComponent"},
    {fieldname: "AverageAge", fieldvalue:"name", fieldtype:"rangesliderComponent"},
    {fieldname: "mostcommonincome", fieldvalue:"name", fieldtype:"dropdownComponent", values: [{value: "No income"},{value: "R 1 - R 4800"},{value: "R 4801 - R 9600"},{value: "R 9601 - R 19200"},{value: "R 19201 - R 38400"},{value: "R 38401 -  R 76800"},{value: "R 76801 - R 153600"},{value: "R 153601 - R 307200"},{value: "R 307201 - R 614400"},{value: "R 614401- R 1228800"},{value: "R 1228801 - R 2457600"},{value: "R2457601 or more"}]},
    {fieldname: "mostcommonhousing", fieldvalue:"name", fieldtype:"dropdownComponent", values: [{value: "Dwelling/house or brick/concrete block structure on a separate stand or yard or on farm"},{value: "Informal dwelling/shack not in backyard, e.g. in an informal/squatter settlement or on farm"},{value: "Traditional dwelling/hut/structure made of traditional materials"},{value: "Informal dwelling/shack in backyard"},{value: "Room/flatlet on a property or a larger dwelling servants' quarters/granny flat"},{value: "Flat or apartment in a block of flats"},{value: "Dwelling/house/flat/room in backyard"},{value: "Semi-detached house"},{value: "Town house (semi-detached house in complex)"},{value: "Other"},{value: "Cluster house in complex"},{value: "Caravan/tent"}]},
    {fieldname: "placename", fieldvalue:"name", fieldtype:"textboxComponent"}
      ]

  constructor(private http: HttpClient,
    private router: Router) { }

  //Returns possible criteria defined above, this goes into the dropdown
  getFilters()
  {
    return this.criteria;
  }

  //Returns HTML for the live map view
  getMap(params)
  {
    return environment.APIURLESCAPED+"/getmap/"+params;
  }

  //Returns URL for the Google Maps search, browser redirects to this
  getGMap(params)
  {
    
    return this.http.get(environment.APIURL+"/getgmaps/"+params)
    .toPromise()

  }

  getResults(searchobject)
  {//Converts search object into a string which will be appended to the end of the GET request, this contains parameters for the DB query 
    var querystring = '/locations/1?';
    var ranges = 'Ranges=';
    for(var i = 0; i<searchobject.length; i++)
    {
      //If it is a range component, convert to the right format in order to allow parsing into JSON on API side
      if(searchobject[i]["fieldtype"] === "rangesliderComponent")
      {
        ranges += searchobject[i]["fieldname"]+","+searchobject[i]["fieldvalue"]+","
      }
      else
      querystring+=searchobject[i]["fieldname"]+"="+searchobject[i]["fieldvalue"]+"&"
    }
    //Else just ad it in the standard format
    if(ranges !== 'Ranges=')
    querystring += ranges;
    querystring= querystring.slice(0, -1)

    return this.http.get<any>(environment.APIURL+querystring).toPromise()
  }
}
